import React from 'react';
import s from './Checkbox.module.scss';

const Checkbox = React.forwardRef(
  ({ label, ...otherProps }: { type: string; label?: string, value: string, onClick: React.MouseEventHandler<HTMLInputElement> }, ref: React.Ref<HTMLInputElement>) => {

    return ( 
      <div className={s.wrapper}>
        <input className={s.checkbox__input} ref={ref} {...otherProps} />
        {label && <label className={s.checkbox__label}>{label}</label>}
      </div>
      
    )
});

export default Checkbox;