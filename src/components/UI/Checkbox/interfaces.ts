export interface ICheckbox {
    label?: string;
    type: string;
    ref: React.Ref<HTMLInputElement>;
    value: string;
    onClick: () => void;
  }