export interface ICustomLink {
    children: React.ReactNode
    className: string;
    link: string;
}