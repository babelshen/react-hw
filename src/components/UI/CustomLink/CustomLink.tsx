import { Link } from 'react-router-dom';
import { ICustomLink } from './interfaces';
import s from './CustomLink.module.scss';

const CustomLink:React.FC<ICustomLink> = ({children, className, link}) => {
    return(
        <Link className={s[className]} to={link}>{children}</Link>
    )
}

export default CustomLink;