import React from 'react';
import { FieldError } from 'react-hook-form';
import warn from '../../../../image/warning.svg'; 
import s from './Input.module.scss';

const Input = React.forwardRef(
  ({ type, label, placeholder, error, ...props}: { type: string; label?: string; placeholder: string; error?: FieldError }, ref: React.Ref<HTMLInputElement>) => {

    const handleBlur = (e:React.FocusEvent<HTMLInputElement>) => {
        if (e.target.value) e.target.classList.add(`${s.active}`)
        else e.target.classList.remove(`${s.active}`)
    }

    return (
      <div className={s.field}>
        {label && <label className={s.input__label}>{label}</label>}
        <div className={s.input__wrapper}>
          <input 
            className={s.input} 
            ref={ref} 
            type={type} 
            placeholder={placeholder}
            {...props}
            onBlur={handleBlur} 
            />
          {error ? <div className={s.input__image}><img className={s.input__image__error} src={warn} alt='Error' title='Error' /></div> : false}
        </div>
        {error ? <div className={s.error__message}>{error.message || 'Error'}</div> : false}
      </div>
    );
  }
);

export default Input;