import { IButton } from './interfaces';
import s from './Button.module.scss'


const Button: React.FC<IButton> = ({type, className, children, onClick}) => {
    return(
        <button className={s[className]} type={type} onClick={onClick}>
            <span className={s.button__text}>{children}</span>
        </button>
    );
};

export default Button;
