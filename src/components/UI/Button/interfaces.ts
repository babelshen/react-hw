export interface IButton {
    type: "button" | "submit" | "reset";
    className: string;
    children: React.ReactNode;
    onClick: () => void;
}