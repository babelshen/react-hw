import { useForm } from "react-hook-form";
import { useNavigate } from "react-router";
import Input from "../../UI/Input";
import Button from "../../UI/Button";
import CustomLink from "../../UI/CustomLink";
import { IForgetPassword } from "./interfaces";
import s from './ForgetPasswordForm.module.scss'


const ForgetPasswordForm:React.FC = () => {
    
    const { register, handleSubmit, formState: {errors}} = useForm<IForgetPassword>({mode: 'onSubmit'});
    const navigate = useNavigate();
    
    const onSubmit = (data: IForgetPassword) => {
      if (data) {
        console.log(data);
        navigate('/reset-password')
      }
    }

    return (
      <form className={s.form} onSubmit={handleSubmit(onSubmit)}>
        <Input 
          type="email" 
          label="Email"  
          placeholder="mail@mail.com"
          error = {errors.email}
          {...register("email", {
            required: 'This field must be filled in', 
            minLength: {
              value: 5,
              message: 'Email is too short. Minimum characters: 5',
            },
            maxLength: {
              value: 25,
              message: 'Email is too long. Maximum characters: 25',
            },
            pattern: {
              value: /^\S+@\S+$/i,
              message: 'Invalid email',
            }
          })} 
        />
        
        <div className={s.buttons}>
          <Button className='button__colorfull' type="submit" onClick={handleSubmit(onSubmit)}>Reset</Button>
          <CustomLink link='/sign-in' className='link__general'>Cancel</CustomLink>
        </div>
      </form>
    );
}

export default ForgetPasswordForm;