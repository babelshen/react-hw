import { useState } from 'react';
import { useForm } from 'react-hook-form';
import Input from '../../UI/Input';
import Checkbox from '../../UI/Checkbox';
import Button from '../../UI/Button/Button';
import { ILoginForm } from './interfaces';
import s from './LoginForm.module.scss';

const LoginForm: React.FC = () => {
  const [seePassword, setSeePassword] = useState<boolean>(false);
  const { register, handleSubmit, formState: {errors}} = useForm<ILoginForm>({mode: 'onBlur'});
  
  const onSubmit = (data: ILoginForm) => console.log(data);

  return (
    <form className={s.form} onSubmit={handleSubmit(onSubmit)}>
        <Input 
          type="email" 
          label="Email"  
          placeholder="mail@mail.com"
          error = {errors.email}
          
          {...register("email", {
            required: 'This field must be filled in', 
            minLength: {
              value: 5,
              message: 'Email is too short. Minimum characters: 5',
            },
            maxLength: {
              value: 25,
              message: 'Email is too long. Maximum characters: 25',
            },
            pattern: {
              value: /^\S+@\S+$/i,
              message: 'Invalid email',
            }
          })} 
        />

        <Input 
          type={seePassword ? 'text' : 'password'} 
          label="Password" 
          placeholder="Password123"
          error = { errors.password }
          {...register("password", {
            required: 'This field must be filled in',
            minLength: {
              value: 6,
              message: 'Password is too short. Minimum characters: 5',
            },
            maxLength: {
              value: 18,
              message: 'Password is too long. Maximum characters: 18',
            },
          })} 
        />

        <Checkbox 
          type="checkbox" 
          label="Show password" 
          value="true"
          onClick={() => setSeePassword(!seePassword)}
          {...register("show")} 
        />

        <Button className='button__colorfull' type="submit" onClick={handleSubmit(onSubmit)}>Login</Button>
    </form>
  );
}

export default LoginForm;

