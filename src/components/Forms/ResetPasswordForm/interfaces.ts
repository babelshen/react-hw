export interface IResetPasswordFormProps  {
    changePassword: boolean;
    setChangePassword: (value: boolean) => void;
}

export interface IResetPasswordForm {
  new_password: string; 
  confirm_password: string; 
  show: boolean;
}