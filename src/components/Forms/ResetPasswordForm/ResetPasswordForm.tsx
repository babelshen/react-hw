import { useState } from "react";
import { useForm } from "react-hook-form";
import Input from "../../UI/Input";
import Checkbox from "../../UI/Checkbox";
import Button from "../../UI/Button";
import { IResetPasswordForm, IResetPasswordFormProps } from "./interfaces";
import s from './ResetPasswordForm.module.scss';

const ResetPasswordForm: React.FC<IResetPasswordFormProps> = ({changePassword, setChangePassword}) => {

    const [seePassword, setSeePassword] = useState<boolean>(false);

    const { register, handleSubmit, formState: {errors}} = useForm<IResetPasswordForm>({mode: 'onBlur'});
    
    const onSubmit = (data: IResetPasswordForm ) => { 
        if (data) {
            console.log(data);
            setChangePassword(!changePassword);
        }
    }
  
    return (
      <form className={s.form} onSubmit={handleSubmit(onSubmit)}>
          <Input 
            type={seePassword ? 'text' : 'password'} 
            label="New Password"  
            placeholder="Password123"
            error = { errors.new_password }
            {...register("new_password", {
            required: 'This field must be filled in',
            minLength: {
              value: 6,
              message: 'Password is too short. Minimum characters: 5',
            },
            maxLength: {
              value: 18,
              message: 'Password is too long. Maximum characters: 18',
            },
            })} 
          />
  
          <Input 
            type={seePassword ? 'text' : 'password'} 
            label="Confirm Password" 
            placeholder="Password123"
            error = { errors.confirm_password }
            {...register("confirm_password", {
              required: 'This field must be filled in',
              minLength: {
                value: 6,
                message: 'Password is too short. Minimum characters: 5',
              },
              maxLength: {
                value: 18,
                message: 'Password is too long. Maximum characters: 18',
              },
            })} 
          />
  
          <Checkbox 
            type="checkbox" 
            label="Show password" 
            value='Yes' 
            onClick={() => setSeePassword(!seePassword)}
            {...register("show")} 
          />
  
          <Button className='button__colorfull' type="submit" onClick={handleSubmit(onSubmit)}>Reset</Button>
      </form>
    );
}

export default ResetPasswordForm;