export interface IRegistration {
    email: string;
    password: string;
    confirm_password: string;
    show: boolean;
}