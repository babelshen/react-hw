import { useForm } from 'react-hook-form';
import { useState } from 'react';
import Input from '../../UI/Input';
import Checkbox from '../../UI/Checkbox';
import Button from '../../UI/Button/Button';
import { IRegistration } from './interfaces';
import s from './RegistrationForm.module.scss';

const RegistrationForm: React.FC = () => {

  const [seePassword, setSeePassword] = useState<boolean>(false);
  const { register, handleSubmit, formState: {errors}} = useForm<IRegistration>({mode: 'onBlur' , shouldUseNativeValidation: true});
  const onSubmit = (data:IRegistration) => console.log(data);
  return (
    <form className={s.form} onSubmit={handleSubmit(onSubmit)}>

        <Input 
          type="email" 
          label="Email" 
          placeholder="mail@mail.com" 
          error = {errors.email}
          {...register("email", {
            required: 'This field must be filled in', 
            minLength: {
              value: 5,
              message: 'Email is too short. Minimum characters: 5',
            },
            maxLength: {
              value: 25,
              message: 'Email is too long. Maximum characters: 25',
            },
            pattern: {
              value: /^\S+@\S+$/i,
              message: 'Invalid email',
            }
          })} 
        />

        <Input 
          type={seePassword ? 'text' : 'password'} 
          label="Password" 
          placeholder="Password123" 
          error = {errors.password}
          {...register("password", {
            required: 'This field must be filled in',
            minLength: {
              value: 6,
              message: 'Password is too short. Minimum characters: 5',
            },
            maxLength: {
              value: 18,
              message: 'Password is too long. Maximum characters: 18',
            },
          })} 
        />

        <Input 
          type={seePassword ? 'text' : 'password'}  
          label="Confirm Password" 
          placeholder="Password123" 
          error = {errors.confirm_password}
          {...register("confirm_password", {
            required: 'This field must be filled in',
            minLength: {
              value: 6,
              message: 'Password is too short. Minimum characters: 5',
            },
            maxLength: {
              value: 18,
              message: 'Password is too long. Maximum characters: 18',
            },
          })} 
        />

        <Checkbox 
          type="checkbox" 
          label="Show password" 
          value='Yes' 
          onClick={() => setSeePassword(!seePassword)} 
          {...register("show")} 
        />

        <Button className='button__colorfull' type="submit" onClick={handleSubmit(onSubmit)}>Register</Button> 
    </form>
  );
}

export default RegistrationForm;