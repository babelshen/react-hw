import logo from '../../../image/logo.svg';
import s from './GuestImage.module.scss'

const GuestImage: React.FC = () => {
    return (
        <div className={s.image__container}>
            <a href='/'>
                <img src={logo} className={s.image} alt='University image' title='University S-PRO' />
            </a>
        </div>
    );   
}

export default GuestImage;