import { RouterProvider } from 'react-router'
import router from './configs/routeConfig'

function App() {

  return (     
    <RouterProvider router={router} />
  )
}

export default App;
