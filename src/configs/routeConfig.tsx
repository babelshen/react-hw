import { createBrowserRouter, createRoutesFromElements, Navigate, Route } from "react-router-dom";
import LoginPage from '../pages/LoginPage';
import ResetPasswordPage from '../pages/ResetPasswordPage';
import RegistrationPage from '../pages/RegistrationPage';
import ForgetPasswordPage from "../pages/ForgetPasswordPage";
import ErrorPage from '../pages/ErrorPage';

export const router = createBrowserRouter(createRoutesFromElements(
    <>
    <Route path="/">
      <Route index element={<Navigate replace to='sign-in' />} />
      <Route path="sign-in" element={<LoginPage />} />
      <Route path="sign-up" element={<RegistrationPage />} />
      <Route path="forget-password" element={<ForgetPasswordPage />} />
      <Route path="reset-password" element={<ResetPasswordPage />} />
    </Route>
    <Route path="*" element={<ErrorPage />} />
    </>
));

export default router;