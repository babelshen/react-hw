import ForgetPasswordForm from "../../components/Forms/ForgetPasswordForm";
import GuestImage from "../../components/GuestImage";
import s from './ForgetPasswordPage.module.scss';

const ForgetPasswordPage = () => {
    return(
        <div className={s.wrapper}>
            <GuestImage />
            <h2 className={s.title}>Reset Password</h2>
            <p className={s.help__text}>Don't worry, happens to the best of us. Enter the email address associated with your account and we'll send you a link to reset.</p>
            <ForgetPasswordForm  />
        </div>
    )
}

export default ForgetPasswordPage;