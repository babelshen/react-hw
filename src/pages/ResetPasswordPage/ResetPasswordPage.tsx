import { useState } from 'react';
import GuestImage from "../../components/GuestImage";
import ResetPasswordForm from '../../components/Forms/ResetPasswordForm/ResetPasswordForm';
import CustomLink from '../../components/UI/CustomLink';
import s from './ResetPasswordPage.module.scss';


const ResetPasswordPage: React.FC = () => {

    const [changePassword, setChangePassword] = useState<boolean>(false);

    return(
        <div className={s.wrapper}>
            <GuestImage />
                { !changePassword
                    ? <>
                        <h2 className={s.title}>Reset Your Password</h2>
                        <ResetPasswordForm changePassword={changePassword} setChangePassword={setChangePassword} />
                      </>
                    : <>
                        <h2 className={s.title}>Password Changed</h2>
                        <p className={s.help__text}>You can use your new password to log into your account</p>
                        <CustomLink className='link__colorfull' link='/sign-in'>Log In</CustomLink> 
                      </>
            }

        </div>
    )
}

export default ResetPasswordPage;