import LoginForm from "../../components/Forms/LogginForm";
import GuestImage from "../../components/GuestImage/GuestImage";
import s from './LoginPage.module.scss';

const LoginPage: React.FC = () => {
    return(
        <div className={s.wrapper}>
            <GuestImage />
            <h2 className={s.title}>Welcome!</h2>
            <LoginForm />
        </div>
    )
}

export default LoginPage;