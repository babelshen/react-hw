import RegistrationForm from "../../components/Forms/RegistrationForm";
import GuestImage from "../../components/GuestImage";
import s from './RegistrationPage.module.scss';

const RegistrationPage: React.FC = () => {
    return(
        <div className={s.wrapper}>
            <GuestImage />
            <h2 className={s.title}>Register your account</h2>
            <RegistrationForm />
        </div>
    )
}

export default RegistrationPage;